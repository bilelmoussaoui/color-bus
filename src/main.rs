use serde::Deserialize;
use zbus::{dbus_proxy, fdo::Result, fdo::DBusProxy, Connection};
use zvariant::{OwnedObjectPath, OwnedValue};
use zvariant_derive::{DeserializeDict, SerializeDict, Type, TypeDict};
use serde_repr::{Deserialize_repr, Serialize_repr};

#[derive(Serialize_repr, Deserialize_repr, PartialEq, Debug, Type)]
#[repr(u32)]
pub enum ResponseType {
    // Success, the request is carried out
    Success = 0,
    // The user cancelled the interaction
    Cancelled = 1,
    // The user interaction was ended in some other way
    Other = 2,
}

#[derive(SerializeDict, DeserializeDict, TypeDict, Debug, Default)]
/// Specified options on a pick color request.
pub struct PickColorOptions {
    /// A string that will be used as the last element of the handle. Must be a valid object path element.
    pub handle_token: Option<String>,
}


#[derive(Debug, Type, Deserialize)]
pub struct ColorResponse(pub ResponseType, pub std::collections::HashMap<String, OwnedValue>);

#[derive(SerializeDict, DeserializeDict, TypeDict, Debug)]
/// Specified options on a screenshot request.
pub struct ColorResult {
    /// A string that will be used as the last element of the handle. Must be a valid object path element.
    pub color: Vec<f64>,
}

#[dbus_proxy(
    interface = "org.freedesktop.portal.Screenshot",
    default_service = "org.freedesktop.portal.Desktop",
    default_path = "/org/freedesktop/portal/desktop"
)]
/// The interface lets sandboxed applications request a screenshot.
trait Screenshot {
    /// Obtains the color of a single pixel.
    ///
    /// Returns a [`Request`] handle
    ///
    /// # Arguments
    ///
    /// * `parent_window` - Identifier for the application window
    /// * `options` - A [`PickColorOptions`]
    ///
    /// [`PickColorOptions`]: ./struct.PickColorOptions.html
    /// [`Request`]: ../request/struct.RequestProxy.html
    fn pick_color(
        &self,
        parent_window: &str,
        options: PickColorOptions,
    ) -> Result<OwnedObjectPath>;
}

pub struct RequestProxy<'a> {
    proxy: DBusProxy<'a>,
    connection: &'a Connection,
}

impl<'a> RequestProxy<'a> {
    pub fn new(connection: &'a Connection, handle: &'a str) -> Result<Self> {
        let proxy = DBusProxy::new_for(connection, handle, "/org/freedesktop/portal/desktop")?;
        Ok(Self { proxy, connection })
    }

    pub fn on_response<F, T>(&self, callback: F) -> Result<()>
    where
        F: FnOnce(T),
        T: serde::de::DeserializeOwned + zvariant::Type,
    {
        loop {
            let msg = self.connection.receive_message()?;
            let msg_header = msg.header()?;
            if msg_header.message_type()? == zbus::MessageType::Signal
                && msg_header.member()? == Some("Response")
            {
                let response = msg.body::<T>()?;
                callback(response);
                break;
            }
        }
        Ok(())
    }
}


fn main () -> zbus::fdo::Result<()> {
    let connection = zbus::Connection::new_session()?;
    let proxy = ScreenshotProxy::new(&connection)?;
    let request_handle = proxy.pick_color(
        "",
        PickColorOptions::default(),
    )?;
    let request = RequestProxy::new(&connection, &request_handle)?;
    request.on_response(|t: ColorResponse| {
        if t.0 == ResponseType::Success {
            println!("{:#?}", t.1.get("color").unwrap().downcast_ref::<zvariant::Structure>());
        }
    })?;
    Ok(())
}